package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/hello")
    public String hello() throws InterruptedException {
        longRunningMethod();
        return "hello";
    }

    public void longRunningMethod() throws InterruptedException {
        Thread.sleep(2000);
    }
}